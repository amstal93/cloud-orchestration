#============
# Project config
#============
variable "google-project" {
    description = "The ID of the google cloud project"
}

variable "google-region" {
    description = "The name of region where google cloud resources will be created"
}

variable "google-zone" {
    description = "A specific zone in a given region"
}

#============
# Compute instance
#============
variable "machine-type" {
    description = "The machine type to create"
}

variable "machine-image" {
    description = "The machine type to create"
}

variable "number-slave" {
    description = "The number of compute instances to create"
}

#=================
# Compute Disk
#=================
variable "disk-size" {
    description = "Boot disk size in GB"
    type        = number 
    default     = 50
}

variable "disk-type" {
    description = "Boot disk type, can be either pd-ssd, local-ssd, or pd-standard"
    type        = string
    default     = "pd-standard"
}

variable "auto-delete" {
    description = "Whether or not the boot disk should be auto-deleted"
    type        = bool
    default     = true
}

variable "device-name" {
    description = "The name or self_link of the disk to attach to this instance"
    type        = string
    default     = ""
}

variable "disk-source" {
    description = "The name or self_link of the disk to attach to this instance"
    type        = string
    default     = null
}

variable "disk-mode" {
    description = "The mode in which to attach this disk"
    type        = string
    default     = "READ_WRITE"
}

variable "additional-disks" {
    description = "List of maps of additional disks"
    type = list(object({
        device_name  = string
        source       = string
        mode         = string
    }))
    default = []
}

#================
# Compute Network
#================
variable "bastion-subnet-ip-cidr" {
    type    = string
    default = ""
}

variable "master-subnet-ip-cidr" {
    type    = string
    default = ""
}

variable "slave-subnet-ip-cidr" {
    type    = string
    default = ""
}

variable "network" {
    description = "The name or self_link of the network to attach this interface to"
}

variable "subnetwork" {
    description = "The name or self_link of the subnetwork to attach this interface to"
}


#===========
# Metada
#==========
variable "ssh-key" {
    type    = string
    default = ""
}

variable "metadata" {
    description = "The Metadata key/value pairs to make available from within the instance"
}
